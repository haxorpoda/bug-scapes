﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bugs
{
    [System.Serializable]
    public class Colors
    {
        public string hex;
        public List<float> hls;
    }

    [System.Serializable]
    public class Dimensions
    {
        public int height;
        public int width;
        public string type;
    }

    [System.Serializable]
    public class Bugs
    {
        public string file;
        public Colors colors;
        public Dimensions dimensions;
    }

    [System.Serializable]
    public class RootObject
    {
        public Bugs[] bugs;
    }


}