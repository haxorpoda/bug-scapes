﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using System.IO;

public class PlaceBugs : MonoBehaviour
{
    public GameObject paintingPlane;
    private GameObject scrollView;
    private GameObject attributionField;

    private Texture2D painting;

    private int paintingHeight;
    private int paintingWidth;
    
    // input start & end are the dimensions of the image (start normally 0)
    public float inputStartX;
    public float inputEndX;
    public float inputStartY;
    public float inputEndY;

    // xBack is close to the panel xFront is closer to the camera
    public float outputStartXBack;
    public float outputStartXFront;
    public float outputEndXBack;
    public float outputEndXFront;

    // yBack is close to the panel yFront is closer to the camera
    public float outputStartYBack;
    public float outputStartYFront;
    public float outputEndYBack;
    public float outputEndYFront;

    public float randomLayer;

    private GameObject mainCamera;
    private Quaternion cameraStartRotation;
    private Vector3 cameraStartPosition;

    List<GameObject> allBugs;

    Bugs.RootObject bugs;
    List<string> metaDataLines;
    string adjustedInventoryNumber;
    int bugCount;

    // Start is called before the first frame update
    void Start()
    {
        allBugs = new List<GameObject>();

        scrollView = GameObject.Find("Scroll View");
        mainCamera = GameObject.Find("Main Camera");
        paintingPlane = GameObject.Find("Painting Plane");
        attributionField = GameObject.Find("AttributionField");

        cameraStartRotation = mainCamera.transform.rotation;
        cameraStartPosition = mainCamera.transform.position;

        TextAsset indexJsonTextAsset = (TextAsset) Resources.Load("color-map");
        TextAsset attributionsTextAsset = (TextAsset) Resources.Load("CodingDaVinci_JMF_corrected");

        bugs = JsonUtility.FromJson<Bugs.RootObject>("{\"bugs\":" + indexJsonTextAsset.text + "}");
        
        metaDataLines = new List<string>();

        using (StringReader reader = new StringReader(attributionsTextAsset.text))
        {
            string line;
            bool firstLine = true;
            while ((line = reader.ReadLine()) != null)
            {
                // Skip the first line with the column names
                if (firstLine)
                {
                    firstLine = false;
                    continue;
                }
                
                // Save the line for later use
                metaDataLines.Add(line);
            }
        }

        // Initialize the painting and reset the camera
        initialize("JMF1994-0007_I-113");
    }

    // Initialize is always called when a new picture should be loaded
    void initialize (string paintingFileName)
    {
        // Destroy all existing bugs and clear the list with the references
        foreach (GameObject bug in allBugs)
        {
            Destroy(bug);
        }

        allBugs.Clear();
        bugCount = 0;

        // Load painting from resources folder, 
        painting = Resources.Load("Paintings/" + paintingFileName) as Texture2D;
        paintingPlane.GetComponent<Renderer>().material.mainTexture = painting;
        paintingHeight = painting.height;
        paintingWidth = painting.width;

        // Set the license
        foreach(string line in metaDataLines)
        {
            string[] fields = line.Split(';');
            adjustedInventoryNumber = fields[0].Replace(" ", "_").Replace("/", "-");

            if (adjustedInventoryNumber.Equals(paintingFileName))
            {
                attributionField.GetComponent<Text>().text = "Attribution: " + fields[0] + " by \"Ludwig Meidner Archiv, Jüdisches Museum der Stadt Frankfurt\" is licensed under CC-BY-SA"; 
                break;
            }
        }

        // Calculate & apply the aspect ratio of painting
        float aspectRatio = (float)paintingWidth / paintingHeight;
        Vector3 paintingPlaneScale = paintingPlane.transform.localScale;
        paintingPlaneScale.x = 2 * aspectRatio;
        paintingPlane.transform.localScale = paintingPlaneScale;

        // Also apply the aspect ration on the x axis
        outputStartXBack = -8f * aspectRatio;
        outputEndXBack = 8f * aspectRatio;

        outputStartXFront = -6f * aspectRatio;
        outputEndXFront = 6f * aspectRatio;

        // The y axis always stays the same
        outputStartYBack = 8;
        outputEndYBack = -8;

        outputStartYFront = 6;
        outputEndYFront = -6;
    }

    // Update is called once per frame
    void Update()
    {
        // Reset the camera position when pressing R
        if (Input.GetKeyDown(KeyCode.R))
        {
            mainCamera.transform.position = cameraStartPosition;
            mainCamera.transform.rotation = cameraStartRotation;
        }

        // Hide the top menue when pressing H
        if (Input.GetKeyDown(KeyCode.H))
        {
            // Toogle the menues active state to make it appear or disappear
            scrollView.SetActive(!scrollView.activeSelf);
        }

        // Capture a screenshot with P
        if (Input.GetKeyDown(KeyCode.P))
        {
            ScreenCapture.CaptureScreenshot(Path.Combine("Screenshots", adjustedInventoryNumber + "_covered_with_" + bugCount + "_bugs_" + System.DateTime.Now.ToString("dd-MM-yy_hh-mm-ss") + ".png"));
        }

        // Only contiue after this point if space is pressed
        if (!Input.GetKey(KeyCode.Space))
        {
            return;
        }

        // Whithout this loop, the code would spawn one bug per frame. This speeds things up on faster machines
        for (int multiplicator = 0; multiplicator < 15; multiplicator++)
        {
            // Select random pixel in image
            int randomX = Random.Range(0, paintingWidth);
            int randomY = Random.Range(0, paintingHeight);

            // Select a random layer on the screen (0 = close to image, 10 = close to camera)
            float randomLayer = Random.Range(0, 10f);

            // Define a range to which the layers are beeing mapp
            float layerStart = 0;
            float layerEnd = 6f;

            // Define the input range of the picture
            inputStartX = 0;
            inputEndX = paintingWidth;

            inputStartY = 0;
            inputEndY = paintingHeight;
            
            // Map the layer value into the mapping bounds
            // This is done to have a mapping function that adjusts according to the proximity to the painting
            // Ex the mapping range gets smaller the closer the layer is to the camera
            float outputStartY = MapV(randomLayer, layerStart, layerEnd, outputStartYBack, outputStartYFront);
            float outputEndY = MapV(randomLayer, layerStart, layerEnd, outputEndYBack, outputEndYFront);
            float outputStartX = MapV(randomLayer, layerStart, layerEnd, outputStartXBack, outputStartXFront);
            float outputEndX = MapV(randomLayer, layerStart, layerEnd, outputEndXBack, outputEndXFront);

            // Only here define the mapping range
            float outputY = MapV(randomY, inputStartY, inputEndY, outputStartY, outputEndY);
            float outputX = MapV(randomX, inputStartX, inputEndX, outputStartX, outputEndX);
            
            // Spawn a new plane on which we are going to draw a bug
            // Note that the layervalue on the z axis gets inverted to spawn the bugs in front of the picture
            GameObject newBugPlane = Instantiate(Resources.Load("BugPlaneParent") as GameObject,
                    new Vector3(outputX, outputY, -randomLayer),
                    Quaternion.identity);

            // Save the Plane to a list for easy deletion
            allBugs.Add(newBugPlane);

            // Flip the on the y axis 
            Color randomPixel = painting.GetPixel(randomX, paintingHeight - randomY);

            // Convert the pixel's RGB colors to HSL
            float randomPixelH;
            float randomPixelS;
            float randomPixelL;

            Color.RGBToHSV(randomPixel, out randomPixelH, out randomPixelS, out randomPixelL);
            
            // Find the best matching bug for the pixels H value
            float bestMatchingBugH = 0;
            float bestMatchingBugS = 0;
            float bestMatchingBugL = 0;
            
            int bestMatchingWidth = 0;
            int bestMatchingHeight = 0;

            string bestMatchingBugFilename = "";

            bool firstRun = true;

            foreach (Bugs.Bugs bug in bugs.bugs)
            {
                // Currently, only the h value is held back
                // TODO: Calculate true sistance between two HSL
                if (firstRun || (Mathf.Abs(randomPixelH - bestMatchingBugH) > Mathf.Abs(randomPixelH - bug.colors.hls[0])))
                {
                    firstRun = false;

                    bestMatchingBugH = bug.colors.hls[0];
                    bestMatchingBugS = bug.colors.hls[1];
                    bestMatchingBugL = bug.colors.hls[2];

                    bestMatchingWidth = bug.dimensions.width;
                    bestMatchingHeight = bug.dimensions.height;

                    bestMatchingBugFilename = bug.file.Replace(".png", "");
                }
            }

            // Also calculate a scaling factor based on the layer value
            // Bugs closer to the painting will be drawn bigger
            float xScale = MapV(randomLayer, layerStart, layerEnd, .1f, .05f);
            float yScale = MapV(randomLayer, layerStart, layerEnd, .1f, .05f);

            newBugPlane.transform.localScale = new Vector3(xScale * bestMatchingWidth / 1000, yScale * bestMatchingHeight / 1000, 1);

            // Load the filname from the resource folder
            Texture2D newBugTexture = Resources.Load("Bugs/" + bestMatchingBugFilename) as Texture2D;
            newBugPlane.transform.GetChild(0).GetComponent<Renderer>().material.mainTexture = newBugTexture;

            bugCount++;
        }
    }

    // Map one range into another
    private float MapV (float input, float input_start, float input_end, float output_start, float output_end)
    {
        float slope = 1.0f * (output_end - output_start) / (input_end - input_start);
        return output_start + slope * (input - input_start);
    }

    // React on button clicks in the UI.
    // Unfortunately, this workaround had to be applied because Unity 2019.2.9f1 didn't want to load UI
    // TODO: Rewrite with an appropriate solution
    public void setPainting()
    {
        GameObject clickedPainting = EventSystem.current.currentSelectedGameObject;
        if (clickedPainting != null)
        {
            // Reinitialize the scene
            initialize(clickedPainting.name);
        }
    }
}
