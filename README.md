# bug-scapes

This software got developed for a presentation about CodingDaVinci at Frankfurter Buchmesse 2019. It has been used as a showcase for how hackatons works. On stage, different versions of the project got loaded to show different stages of development.

The paintings used in the screenshots are from the “Ludwig Meidner Archiv, Jüdisches Museum der Stadt Frankfurt” and licensed under CC-BY-SA. The bugs are from the Museum für Naturkunde Berlin and licensed under CC0 (http://gbif.naturkundemuseum-berlin.de/hackathon/Insektenkasten/)

The datasets are pretty big and therefore currently not part of the repository. A download location will be added to this README as soon as I get to it.

**Screenshots**
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_0_bugs_21-10-19_10-44-17.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_330_bugs_21-10-19_10-44-30.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_2280_bugs_21-10-19_10-44-33.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_5985_bugs_21-10-19_10-44-38.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_12120_bugs_21-10-19_10-44-55.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_12120_bugs_21-10-19_10-45-01.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_12120_bugs_21-10-19_10-45-07.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_12120_bugs_21-10-19_10-45-37.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_12120_bugs_21-10-19_10-46-06.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-113_covered_with_12120_bugs_21-10-19_10-45-18.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-157_covered_with_0_bugs_21-10-19_10-47-10.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-157_covered_with_690_bugs_21-10-19_10-47-13.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-157_covered_with_3810_bugs_21-10-19_10-47-17.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-157_covered_with_10680_bugs_21-10-19_10-47-29.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-157_covered_with_10680_bugs_21-10-19_10-47-56.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-157_covered_with_10680_bugs_21-10-19_10-48-18.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-183_covered_with_0_bugs_21-10-19_10-50-33.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-183_covered_with_225_bugs_21-10-19_10-49-27.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-183_covered_with_900_bugs_21-10-19_10-49-28.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-183_covered_with_8085_bugs_21-10-19_10-49-38.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-183_covered_with_8085_bugs_21-10-19_10-49-54.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-190_covered_with_0_bugs_21-10-19_11-52-28.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-190_covered_with_270_bugs_21-10-19_11-52-34.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-190_covered_with_795_bugs_21-10-19_11-52-35.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-190_covered_with_2040_bugs_21-10-19_11-52-37.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-190_covered_with_9030_bugs_21-10-19_11-52-48.png "Screenshot")
![alt text](Screenshots/JMF1994-0007_I-190_covered_with_9030_bugs_21-10-19_11-52-57.png "Screenshot")
